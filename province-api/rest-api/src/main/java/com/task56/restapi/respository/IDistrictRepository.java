package com.task56.restapi.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task56.restapi.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Long> {
    List<CDistrict> findByProvinceId(Long provinceId);
}
