package com.task56.restapi.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task56.restapi.model.CWard;

public interface IWardRespository extends JpaRepository<CWard, Long> {
    List<CWard> findByDistrictId(Long districtId);
}
