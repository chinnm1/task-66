package com.task56.restapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56.restapi.model.CDistrict;
import com.task56.restapi.model.CWard;
import com.task56.restapi.respository.IDistrictRepository;
import com.task56.restapi.respository.IWardRespository;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class WardController {
    @Autowired
    private IWardRespository wardRespository;
    @Autowired
    private IDistrictRepository districtRepository;

    @PostMapping("/ward/create/{id}")
    public ResponseEntity<Object> createWard(@PathVariable("id") Long id, @RequestBody CWard cWard) {
        try {
            Optional<CDistrict> districtData = districtRepository.findById(id);
            if (districtData.isPresent()) {

                CWard newRole = new CWard();
                newRole.setNameWard(cWard.getNameWard());
                ;
                newRole.setPrefixWard(cWard.getPrefixWard());

                CDistrict _district = districtData.get();
                newRole.setDistrict(_district);
                // newRole.setCountryName(_district.getCountryName());
                // newRole.setId(_district.getId());

                CWard savedRole = wardRespository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/ward/update/{id}")
    public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CWard cWard) {
        Optional<CWard> warData = wardRespository.findById(id);

        if (warData.isPresent()) {
            CWard newWard = warData.get();
            newWard.setNameWard(cWard.getNameWard());
            newWard.setPrefixWard(cWard.getPrefixWard());

            CWard saveWard = wardRespository.save(newWard);
            return new ResponseEntity<>(saveWard, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/ward/details/{id}")
    public CWard getWardById(@PathVariable Long id) {
        if (wardRespository.findById(id).isPresent())
            return wardRespository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/ward/all")
    public List<CWard> getAllWard() {
        return wardRespository.findAll();
    }

    @GetMapping("/ward/{districtId}/wards")
    public List<CWard> getWardByDistrictId(@PathVariable(value = "districtId") Long districtId) {
        return wardRespository.findByDistrictId(districtId);
    }

    @DeleteMapping("/ward/delete/{id}")
    public ResponseEntity<Object> deleteWardById(@PathVariable Long id) {
        try {
            Optional<CWard> optional = wardRespository.findById(id);
            if (optional.isPresent()) {
                wardRespository.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
