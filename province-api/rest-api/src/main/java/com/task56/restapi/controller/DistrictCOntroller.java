package com.task56.restapi.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56.restapi.model.CDistrict;
import com.task56.restapi.model.CProvince;
import com.task56.restapi.model.CWard;
import com.task56.restapi.respository.IDistrictRepository;
import com.task56.restapi.respository.IProvinceRespository;
// import com.task56.restapi.service.DistrictService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DistrictCOntroller {
    // @Autowired
    // private DistrictService districtService;
    @Autowired
    private IProvinceRespository provinceRespository;
    @Autowired
    private IDistrictRepository districtRepository;

    // @GetMapping("/districts")
    // public ResponseEntity<List<CDistrict>> getAllDistrict() {
    // try {
    // return new ResponseEntity<>(districtService.getAllDistrict(), HttpStatus.OK);
    // } catch (Exception e) {
    // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    // }
    // }

    // @GetMapping("/ward")
    // public ResponseEntity<List<CWard>> getWardByDistrictId(
    // @RequestParam(value = "districtId") long districtId) {
    // try {
    // List<CWard> ward = districtService.getWarByDistrictId(districtId);
    // if (ward != null) {
    // return new ResponseEntity<>(ward, HttpStatus.OK);
    // } else {
    // return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    // }
    // } catch (Exception e) {
    // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    // }
    // }

    @PostMapping("/distrcit/create/{id}")
    public ResponseEntity<Object> createWard(@PathVariable("id") Long id, @RequestBody CDistrict cRegion) {
        try {
            Optional<CProvince> countryData = provinceRespository.findById(id);
            if (countryData.isPresent()) {
                System.out.println("find out province");
                CDistrict newRole = new CDistrict();
                newRole.setName(cRegion.getName());
                newRole.setPrefix(cRegion.getPrefix());
                newRole.setWards(cRegion.getWards());
                CProvince _country = countryData.get();
                newRole.setProvince(_country);
                // newRole.setCountryName(_country.getCountryName());
                // newRole.setId(_country.getId());

                CDistrict savedRole = districtRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/district/update/{id}")
    public ResponseEntity<Object> updateWard(@PathVariable("id") Long id, @RequestBody CDistrict cDistrict) {
        Optional<CDistrict> districtData = districtRepository.findById(id);

        if (districtData.isPresent()) {
            CDistrict newDistrict = districtData.get();
            newDistrict.setName(cDistrict.getName());
            newDistrict.setPrefix(cDistrict.getPrefix());

            CDistrict saveDistrict = districtRepository.save(newDistrict);
            return new ResponseEntity<>(saveDistrict, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/district/details/{id}")
    public CDistrict getDistrictById(@PathVariable Long id) {
        if (districtRepository.findById(id).isPresent())
            return districtRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/district/all")
    public List<CDistrict> getAllDistrict() {
        return districtRepository.findAll();
    }

    @GetMapping("/province/{provinceId}/district")
    public List<CDistrict> getRegionsByCountry(@PathVariable(value = "provinceId") Long provinceId) {
        return districtRepository.findByProvinceId(provinceId);
    }

    // @GetMapping("/country/{countryId}/regions/{id}")
    // public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value =
    // "countryId") Long countryId,
    // @PathVariable(value = "id") Long regionId) {
    // return regionRepository.findByIdAndCountryId(regionId, countryId);
    // }

    @DeleteMapping("/district/delete/{id}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable Long id) {
        try {
            Optional<CDistrict> optional = districtRepository.findById(id);
            if (optional.isPresent()) {
                districtRepository.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
