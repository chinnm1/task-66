package com.task56.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.hibernate.service.internal.ProvidedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56.restapi.model.CDistrict;
import com.task56.restapi.model.CProvince;
import com.task56.restapi.respository.IProvinceRespository;
// import com.task56.restapi.service.ProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProvinceController {
    // @Autowired
    // private ProvinceService providedService;
    @Autowired
    private IProvinceRespository provinceRespository;

    // @GetMapping("/provinces")
    // public ResponseEntity<List<CProvince>> getAllProvince() {
    // try {
    // return new ResponseEntity<>(providedService.getAllProvince(), HttpStatus.OK);
    // } catch (Exception e) {
    // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    // }
    // }

    // @GetMapping("/province5")
    // public ResponseEntity<List<CProvince>> getFiveProvince(
    // @RequestParam(value = "page", defaultValue = "1") String page,
    // @RequestParam(value = "size", defaultValue = "5") String size) {
    // try {
    // Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page),
    // Integer.parseInt(size));
    // List<CProvince> list = new ArrayList<CProvince>();
    // provinceRespository.findAll(pageWithFiveElements).forEach(list::add);
    // return new ResponseEntity<>(list, HttpStatus.OK);
    // } catch (Exception e) {
    // return null;
    // }
    // }

    // @GetMapping("/district")
    // public ResponseEntity<List<CDistrict>> getDistrictByprovinceId(
    // @RequestParam(value = "provinceId") long provinceId) {
    // try {
    // List<CDistrict> district =
    // providedService.getDistrictByProvinceId(provinceId);
    // if (district != null) {
    // return new ResponseEntity<>(district, HttpStatus.OK);
    // } else {
    // return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    // }
    // } catch (Exception e) {
    // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    // }
    // }

    @PostMapping("/province/create")
    public ResponseEntity<Object> createCountry(@RequestBody CProvince cCProvince) {
        try {
            CProvince newProvince = new CProvince();
            newProvince.setNameProvince(cCProvince.getNameProvince());
            newProvince.setCodeProvince(cCProvince.getCodeProvince());
            // newProvince.setDistricts(cCProvince.getDistricts());
            System.out.println("check newprovine" + newProvince);
            CProvince saveProvince = provinceRespository.save(newProvince);

            return new ResponseEntity<>(saveProvince, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    @CrossOrigin
    @PutMapping("/province/update/{id}")
    public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CProvince cProvince) {
        Optional<CProvince> provinceData = provinceRespository.findById(id);

        if (provinceData.isPresent()) {
            CProvince nnewProvincw = provinceData.get();
            nnewProvincw.setNameProvince(cProvince.getNameProvince());
            nnewProvincw.setCodeProvince(cProvince.getCodeProvince());

            CProvince saveProvince = provinceRespository.save(nnewProvincw);
            return new ResponseEntity<>(saveProvince, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/province/details/{id}")
    public CProvince getProvinceById(@PathVariable Long id) {
        if (provinceRespository.findById(id).isPresent())
            return provinceRespository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/province")
    public List<CProvince> getAllProvinces() {
        List<CProvince> listProvince = new ArrayList<CProvince>();
        provinceRespository.findAll().forEach(listProvince::add);
        return listProvince;
    }

    @DeleteMapping("/province/delete/{id}")
    public ResponseEntity<Object> deleteProvinceById(@PathVariable Long id) {
        try {
            Optional<CProvince> optional = provinceRespository.findById(id);
            if (optional.isPresent()) {
                provinceRespository.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
