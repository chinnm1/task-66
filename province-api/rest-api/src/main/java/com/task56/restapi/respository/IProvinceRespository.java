package com.task56.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task56.restapi.model.CProvince;

public interface IProvinceRespository extends JpaRepository<CProvince, Long> {
    // CProvince findByProvinceId(long provinceId);

}
